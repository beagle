//
// NautilusMetadata.cs
//
// Copyright (C) 2006 Novell, Inc.
//

//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;

using Beagle;
using Beagle.Util;

namespace Beagle.Daemon.FileSystemQueryable {

	public class NautilusMetadata {

		// Static class
		private NautilusMetadata () { }

		private static FileSystemQueryable queryable = null;

		public static void WatchForChanges (FileSystemQueryable fsq)
		{
			queryable = fsq;

			if (!Inotify.Enabled)
				return;

			string metafile_path = Path.Combine (Environment.GetEnvironmentVariable ("HOME"),
							     ".nautilus/metafiles");

			// FIXME: If the directory doesn't exist, we should
			// probably watch for its creation.
			if (Directory.Exists (metafile_path)) {
				// Nautilus writes the metafile info out into a
				// tempfile and then moves it over top of the
				// old one.

				Inotify.Subscribe (metafile_path,
						   OnInotifyEvent,
						   Inotify.EventType.MovedTo);
			}
		}

		private static void OnInotifyEvent (Inotify.Watch     watch,
						    string            path,
						    string            subitem,
						    string            srcpath,
						    Inotify.EventType type)
		{
			Log.Debug ("Got event {0} on {1} {2}", type, path, subitem);

			if (!subitem.StartsWith ("file:%2F%2F%2F"))
				return;

			// Get the directory by moving past the URI part,
			// sanitizing the path, and removing the extension.
			string item = subitem.Substring (11, subitem.Length - 11 - 4).Replace ("%2F", "/");
			Log.Debug ("item: {0}", item);

			foreach (string file in NautilusTools.GetFiles (item)) {
				Log.Debug ("file: {0}", file);
				
				Guid unique_id = queryable.FilePathToId (file);

				if (unique_id == Guid.Empty) {
					Log.Warn ("Could not resolve unique id of '{0}' for property rewrite", file);
					continue;
				}

				Indexable indexable = new Indexable (GuidFu.ToUri (unique_id));
				indexable.Type = IndexableType.PropertyChange;

				AddPropertiesToIndexable (indexable, file);

				Scheduler.Task task = queryable.NewAddTask (indexable);
				task.Priority = Scheduler.Priority.Immediate;
				queryable.ThisScheduler.Add (task);
			}
		}

		public static void AddPropertiesToIndexable (Indexable indexable,
							     string    path)
		{
			Property prop;
			string val;

			// Since this is often used for resetting properties,
			// always make sure we at least create a property with
			// a String.Empty value.

			val = NautilusTools.GetEmblem (path);
			prop = Property.NewKeyword ("fixme:emblem", val != null ? val : String.Empty);
			prop.IsMutable = true;
			indexable.AddProperty (prop);

			val = NautilusTools.GetNotes (path);
			prop = Property.New ("fixme:note", val != null ? val : String.Empty);
			prop.IsMutable = true; 
			indexable.AddProperty (prop);
		}
	}
}
