# Beagle translation to Serbian
# This file is distributed under the same license as the Beagle package.
# Filip Miletic <filmil at gmail dot com>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: beagle 0.2.10\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-09-25 10:40+0200\n"
"PO-Revision-Date: 2006-09-22 18:28+0200\n"
"Last-Translator: Filip Miletić <filmil at gmail dot com>\n"
"Language-Team: Serbian (sr) <serbiangnome-lista@nongnu.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3;    plural=n%10==1 && n%100!=11 ? 0 :    n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: ../glue/eggtrayicon.c:127
msgid "Orientation"
msgstr "Usmerenje"

#: ../glue/eggtrayicon.c:128
msgid "The orientation of the tray."
msgstr "Usmerenje alatne trake."

#: ../ContactViewer/ContactWindow.cs:74
msgid "Open..."
msgstr "Otvori..."

#: ../ContactViewer/ContactWindow.cs:76 ../search/Tray/TrayIcon.cs:86
msgid "Quit"
msgstr "Kraj"

#: ../ContactViewer/ContactWindow.cs:79
msgid "About"
msgstr "O programu"

#: ../ContactViewer/ContactWindow.cs:95 ../search/Tiles/Utils.cs:26
msgid "Contacts"
msgstr "Kontakti"

#: ../ContactViewer/ContactWindow.cs:100
msgid "Display name"
msgstr "Prikazano ime"

#: ../ContactViewer/ContactWindow.cs:101
msgid "Primary E-mail"
msgstr "Glavna el. adresa"

#: ../ContactViewer/ContactWindow.cs:102
msgid "Secondary E-mail"
msgstr "Druga el. adresa"

#: ../ContactViewer/ContactWindow.cs:103
msgid "Nickname"
msgstr "Nadimak"

#: ../ContactViewer/ContactWindow.cs:133
#, csharp-format
msgid ""
"Unable to open mork database:\n"
"\n"
" {0}"
msgstr ""
"Nije moguće otvoriti radnu bazu podataka:\n"
"\n"
" {0}"

#: ../ContactViewer/ContactWindow.cs:153
msgid "The specified ID does not exist in this database!"
msgstr "Zadati identifikator ne postoji u ovoj bazi podataka!"

#: ../ContactViewer/ContactWindow.cs:180
#, csharp-format
msgid "Added {0} contact"
msgid_plural "Added {0} contacts"
msgstr[0] "Dodat {0} kontakt"
msgstr[1] "Dodata {0} kontakta"
msgstr[2] "Dodato {0} kontakata "

#: ../ContactViewer/ContactWindow.cs:204
#, csharp-format
msgid "Viewing {0}"
msgstr "Pregled: {0}"

#: ../ContactViewer/ContactWindow.cs:205
msgid "DisplayName"
msgstr "PrikazanoIme"

#: ../ContactViewer/ContactWindow.cs:244
msgid "Select a mork database file"
msgstr "Izaberite ime radne baze podataka"

#: ../ContactViewer/ContactWindow.cs:303
msgid "FirstName"
msgstr "Ime"

#: ../ContactViewer/ContactWindow.cs:303
msgid "NickName"
msgstr "Nadimak"

#: ../ContactViewer/ContactWindow.cs:303
msgid "LastName"
msgstr "Prezime"

#: ../ContactViewer/ContactWindow.cs:311
msgid "Primary E-Mail:"
msgstr "Glavna el. adresa:"

#: ../ContactViewer/ContactWindow.cs:313
msgid "PrimaryEmail"
msgstr "GlavnaElAdresa"

#: ../ContactViewer/ContactWindow.cs:315
#: ../ContactViewer/contactviewer.glade.h:33
msgid "Screen name:"
msgstr "Ekransko ime:"

#: ../ContactViewer/ContactWindow.cs:317
msgid "_AimScreenName"
msgstr "Ime na _AIMu"

#: ../ContactViewer/ContactWindow.cs:319
msgid "Home phone:"
msgstr "Kućni tel.:"

#: ../ContactViewer/ContactWindow.cs:321
msgid "HomePhone"
msgstr "KućniTel"

#: ../ContactViewer/ContactWindow.cs:323
msgid "Mobile phone:"
msgstr "Mobilni tel.:"

#: ../ContactViewer/ContactWindow.cs:325
msgid "CellularNumber"
msgstr "MobilniTel"

#: ../ContactViewer/ContactWindow.cs:327
msgid "Web page:"
msgstr "Mrežna strana:"

#: ../ContactViewer/ContactWindow.cs:329
msgid "WebPage2"
msgstr "DrugaStrana"

#: ../ContactViewer/ContactWindow.cs:336
msgid "Send E-Mail"
msgstr "Slanje el. pošte"

#: ../ContactViewer/ContactWindow.cs:340
msgid "Details..."
msgstr "Detalji..."

#: ../ContactViewer/ContactWindow.cs:368
msgid "Could not find a valid E-mail address!"
msgstr "Nije moguće pronaći ispravnu el. adresu!"

#: ../ContactViewer/contactviewer.glade.h:1
msgid "<b>Custom fields</b>"
msgstr "<b>Proizvoljna polja</b>"

#: ../ContactViewer/contactviewer.glade.h:2
msgid "<b>Home</b>"
msgstr "<b>Dom</b>"

#: ../ContactViewer/contactviewer.glade.h:3
msgid "<b>Internet</b>"
msgstr "<b>Internet</b>"

#: ../ContactViewer/contactviewer.glade.h:4
msgid "<b>Name</b>"
msgstr "<b>Ime</b>"

#: ../ContactViewer/contactviewer.glade.h:5
msgid "<b>Notes</b>"
msgstr "<b>Beleške</b>"

#: ../ContactViewer/contactviewer.glade.h:6
msgid "<b>Phones</b>"
msgstr "<b>Telefoni</b>"

#: ../ContactViewer/contactviewer.glade.h:7
msgid "<b>Work</b>"
msgstr "<b>Posao</b>"

#: ../ContactViewer/contactviewer.glade.h:8
msgid "Additional E-Mail:"
msgstr "Dodatna el. adresa:"

#: ../ContactViewer/contactviewer.glade.h:9
msgid "Address"
msgstr "Adresa"

#: ../ContactViewer/contactviewer.glade.h:10
msgid "Address:"
msgstr "Adresa:"

#: ../ContactViewer/contactviewer.glade.h:11
msgid "City:"
msgstr "Mesto:"

#: ../ContactViewer/contactviewer.glade.h:12
msgid "Contact"
msgstr "Kontakt"

#: ../ContactViewer/contactviewer.glade.h:13
msgid "Contact Viewer"
msgstr "Pregled kontakata"

#: ../ContactViewer/contactviewer.glade.h:14
msgid "Country:"
msgstr "Država:"

#: ../ContactViewer/contactviewer.glade.h:15
msgid "Custom 1:"
msgstr "Proizvoljno 1:"

#: ../ContactViewer/contactviewer.glade.h:16
msgid "Custom 2:"
msgstr "Proizvoljno 2:"

#: ../ContactViewer/contactviewer.glade.h:17
msgid "Custom 3:"
msgstr "Proizvoljno 3:"

#: ../ContactViewer/contactviewer.glade.h:18
msgid "Custom 4:"
msgstr "Proizvoljno 4:"

#: ../ContactViewer/contactviewer.glade.h:19
msgid "Department:"
msgstr "Odeljenje:"

#: ../ContactViewer/contactviewer.glade.h:20
msgid "Detailed view"
msgstr "Detaljan pregled"

#: ../ContactViewer/contactviewer.glade.h:21
msgid "Display:"
msgstr "Prikaz:"

#: ../ContactViewer/contactviewer.glade.h:22 ../search/Tiles/Contact.cs:68
msgid "E-Mail:"
msgstr "El. pošta:"

#: ../ContactViewer/contactviewer.glade.h:23
msgid "Fax:"
msgstr "Faks:"

#: ../ContactViewer/contactviewer.glade.h:24
msgid "First:"
msgstr "Prvi:"

#: ../ContactViewer/contactviewer.glade.h:25
msgid "Home:"
msgstr "Kuća:"

#: ../ContactViewer/contactviewer.glade.h:26
msgid "Last:"
msgstr "Poslednji:"

#: ../ContactViewer/contactviewer.glade.h:27
msgid "Mobile:"
msgstr "Mobilni:"

#: ../ContactViewer/contactviewer.glade.h:28
msgid "Nickname:"
msgstr "Nadimak:"

#: ../ContactViewer/contactviewer.glade.h:29
msgid "Organization:"
msgstr "Ustanova:"

#: ../ContactViewer/contactviewer.glade.h:30
msgid "Other"
msgstr "Ostalo"

#: ../ContactViewer/contactviewer.glade.h:31
msgid "Pager:"
msgstr "Pejdžer:"

#: ../ContactViewer/contactviewer.glade.h:32
msgid "Preferred format:"
msgstr "Željeni format:"

#: ../ContactViewer/contactviewer.glade.h:34
msgid "State/Province"
msgstr "Država/pokrajina"

#: ../ContactViewer/contactviewer.glade.h:35
msgid "State/Province:"
msgstr "Država/pokrajina:"

#: ../ContactViewer/contactviewer.glade.h:36 ../search/Tiles/Calendar.cs:51
#: ../search/Tiles/File.cs:124 ../search/Tiles/Note.cs:57
#: ../search/Tiles/RSSFeed.cs:45 ../search/Tiles/WebHistory.cs:50
msgid "Title:"
msgstr "Naslov:"

#: ../ContactViewer/contactviewer.glade.h:37
msgid ""
"Unknown\n"
"Plain text\n"
"HTML"
msgstr ""
"Nepoznato\n"
"tekst\n"
"html"

#: ../ContactViewer/contactviewer.glade.h:40
msgid "Web Page:"
msgstr "Mrežna strana:"

#: ../ContactViewer/contactviewer.glade.h:41
msgid "Work:"
msgstr "Posao:"

#: ../ContactViewer/contactviewer.glade.h:42
msgid "ZIP/Postal Code"
msgstr "Poštanski broj"

#: ../ContactViewer/contactviewer.glade.h:43
msgid "ZIP/Postal Code:"
msgstr "Poštanski broj:"

#: ../ImLogViewer/ImLogViewer.glade.h:1
msgid "<b>Conversations History</b>"
msgstr "<b>Istorijat razgovora</b>"

#: ../ImLogViewer/ImLogViewer.glade.h:2
msgid "IM Viewer"
msgstr "Pregled brzih poruka"

#: ../ImLogViewer/ImLogWindow.cs:102
#, csharp-format
msgid "Conversations in {0}"
msgstr "Razgovori: {0}"

#: ../ImLogViewer/ImLogWindow.cs:109
#, csharp-format
msgid "Conversations with {0}"
msgstr "Razgovor: {0}"

#: ../search/beagle-search.desktop.in.h:1 ../tools/settings.glade.h:28
msgid "Search"
msgstr "Traži"

#: ../search/beagle-search.desktop.in.h:2
msgid "Search for data on your desktop"
msgstr "Pretraga podataka u radnom okruženju"

#: ../search/Category.cs:109
#, csharp-format
msgid "{0} result"
msgid_plural "{0} results"
msgstr[0] "{0} pogodak"
msgstr[1] "{0} pogotka"
msgstr[2] "{0} pogodaka"

#: ../search/Category.cs:111
#, csharp-format
msgid "{0}-{1} of {2}"
msgstr "{0}-{1} od {2}"

#: ../search/Pages/NoMatch.cs:19
msgid "No results were found."
msgstr "Nije pronađen nijedan pogodak."

#: ../search/Pages/NoMatch.cs:20
#, csharp-format
msgid "Your search for \"{0}\" did not match any files on your computer."
msgstr ""
"Pretraga sa ključnim rečima „{0}‟ ne odgovara nijednoj datoteci u skupu koji "
"se pretražuje."

#: ../search/Pages/NoMatch.cs:24
msgid ""
"You can change the scope of your search using the \"Search\" menu. A broader "
"search scope might produce more results."
msgstr ""
"Možete da promenite opseg pretrage upotrebom menija „Traži‟. Proširenje "
"opsega može da ima više pogodaka."

#: ../search/Pages/NoMatch.cs:26
msgid ""
"You should check the spelling of your search words to see if you "
"accidentally misspelled any words."
msgstr "Proverite da ste sigurno ispravno zapisali sve ključne reči."

#: ../search/Pages/QuickTips.cs:10
msgid "You can use upper and lower case; search is case-insensitive."
msgstr ""
"Potrebno je da koristite odgovarajuća mala i velika slova. Mala i velika "
"slova se razlikuju u pretrazi."

#: ../search/Pages/QuickTips.cs:11
msgid "To search for optional terms, use OR.  ex: <b>George OR Ringo</b>"
msgstr ""
"Koristite veznik „OR‟ za opcione reči. Na primer: <b>Ivica OR Marica</b>"

#: ../search/Pages/QuickTips.cs:12
msgid ""
"To exclude search terms, use the minus symbol in front, such as <b>-cats</b>"
msgstr ""
"Da isključite reči iz pretrage koristite znak minus, poput: <b>-mačke</b>"

#: ../search/Pages/QuickTips.cs:13
msgid ""
"When searching for a phrase, add quotes. ex: <b>\"There be dragons\"</b>"
msgstr ""
"Kada tražite frazu, uokvirite je navodnicima: <b>\"Zmaju nemoj sad da kineš "
"inače odosmo na mesec\"</b>"

#: ../search/Pages/QuickTips.cs:19 ../search/UIManager.cs:83
msgid "Quick Tips"
msgstr "Brza pomoć"

#: ../search/Pages/RootUser.cs:12
msgid "Beagle cannot be run as root"
msgstr "Administrator ne može da pokrene Bigla"

#: ../search/Pages/RootUser.cs:13
msgid ""
"For security reasons, Beagle cannot be run as root.  You should restart as a "
"regular user."
msgstr ""
"Iz bezbednosnih razloga administrator ne može da pokreće Bigla.  Potrebno je "
"da program pokrenete kao običan korisnik."

#: ../search/Pages/StartDaemon.cs:19
msgid "Search service not running"
msgstr "Pretraga nije pokrenuta"

#: ../search/Pages/StartDaemon.cs:21
msgid "Start search service"
msgstr "Pokreni pretragu"

#: ../search/Search.cs:149
msgid "_Find:"
msgstr "_Nađi:"

#: ../search/Search.cs:164
msgid "Find Now"
msgstr "Nađi"

#: ../search/Search.cs:210
msgid "Type in search terms"
msgstr "Unesite ključne reči za traženje"

#: ../search/Search.cs:211
msgid "Start searching"
msgstr "Traži"

#: ../search/Tiles/Calendar.cs:54
msgid "Description:"
msgstr "Opis:"

#: ../search/Tiles/Calendar.cs:57
msgid "Location:"
msgstr "Mesto:"

#: ../search/Tiles/Calendar.cs:61
msgid "Attendees:"
msgstr "Prisutni:"

#: ../search/Tiles/Contact.cs:30
msgid "Send Mail"
msgstr "Slanje pošte"

#: ../search/Tiles/Contact.cs:70
msgid "Mobile Phone:"
msgstr "Mobilni tel.:"

#: ../search/Tiles/Contact.cs:72
msgid "Work Phone:"
msgstr "Tel. na poslu:"

#: ../search/Tiles/Contact.cs:74
msgid "Home Phone:"
msgstr "Kućni tel.:"

#: ../search/Tiles/File.cs:34
msgid "Reveal in Folder"
msgstr "Otkrij u fascikli"

#: ../search/Tiles/File.cs:35
msgid "E-Mail"
msgstr "El. pošta"

#. AddAction (new TileAction (Catalog.GetString ("Instant-Message"), InstantMessage));
#. FIXME: s/"gtk-info"/Gtk.Stock.Info/ when we can depend on gtk# 2.8
#. AddAction (new TileAction (Catalog.GetString ("Show Information"), "gtk-info", ShowInformation));
#: ../search/Tiles/File.cs:37 ../search/Tiles/Folder.cs:38
msgid "Move to Trash"
msgstr "Premesti u smeće"

#: ../search/Tiles/File.cs:125 ../search/Tiles/Folder.cs:51
#: ../search/Tiles/Note.cs:58
msgid "Last Edited:"
msgstr "Poslednja izmena:"

#: ../search/Tiles/File.cs:128
msgid "Author:"
msgstr "Autor:"

#: ../search/Tiles/File.cs:130 ../search/Tiles/Folder.cs:52
#: ../search/Tiles/Image.cs:79
msgid "Full Path:"
msgstr "Puna staza:"

#: ../search/Tiles/Folder.cs:31
msgid "Empty"
msgstr "Prazno"

#: ../search/Tiles/Folder.cs:33
#, csharp-format
msgid "Contains {0} Item"
msgid_plural "Contains {0} Items"
msgstr[0] "Sadrži {0} stavku"
msgstr[1] "Sadrži {0} stavke"
msgstr[2] "Sadrži {0} stavki"

#. AddAction (new TileAction (Catalog.GetString ("Add to Library"), Gtk.Stock.Add, AddToLibrary));
#: ../search/Tiles/Image.cs:39
msgid "Set as Wallpaper"
msgstr "Postavi kao pozadinu"

#: ../search/Tiles/Image.cs:78
msgid "Modified:"
msgstr "Izmena:"

#: ../search/Tiles/IMLog.cs:30
msgid "IM Conversation"
msgstr "Razgovor"

#: ../search/Tiles/IMLog.cs:92
msgid "Name:"
msgstr "Ime:"

#: ../search/Tiles/IMLog.cs:93 ../search/Tiles/MailMessage.cs:114
msgid "Date Received:"
msgstr "Datum prijema:"

#: ../search/Tiles/IMLog.cs:97
msgid "Status:"
msgstr "Stanje:"

#: ../search/Tiles/IMLog.cs:112
msgid "Idle"
msgstr "Besposlen"

#: ../search/Tiles/IMLog.cs:119
msgid "Away"
msgstr "Odsutan"

#: ../search/Tiles/IMLog.cs:122
msgid "Offline"
msgstr "Isključen"

#: ../search/Tiles/IMLog.cs:125
msgid "Available"
msgstr "Dostupan"

#: ../search/Tiles/MailAttachment.cs:45
msgid "Mail attachment"
msgstr "Prilog el. poruci"

#: ../search/Tiles/MailMessage.cs:67
msgid "(untitled)"
msgstr "(bez naslova)"

#: ../search/Tiles/MailMessage.cs:77
msgid "Send in Mail"
msgstr "Pošalji poštom"

#: ../search/Tiles/MailMessage.cs:109
msgid "Subject:"
msgstr "Tema:"

#: ../search/Tiles/MailMessage.cs:111
msgid "To:"
msgstr "Prima:"

#: ../search/Tiles/MailMessage.cs:111
msgid "From:"
msgstr "Šalje:"

#: ../search/Tiles/MailMessage.cs:114
msgid "Date Sent:"
msgstr "Datum slanja:"

#: ../search/Tiles/MailMessage.cs:120
msgid "Folder:"
msgstr "Direktorijum:"

#: ../search/Tiles/OpenWithMenu.cs:47
msgid "Open With"
msgstr "Otvori pomoću"

#: ../search/Tiles/Presentation.cs:33
#, csharp-format
msgid "{0} slide"
msgid_plural "{0} slides"
msgstr[0] "{0} slajd"
msgstr[1] "{0} slajda"
msgstr[2] "{0} slajdova"

#: ../search/Tiles/RSSFeed.cs:46
msgid "Site:"
msgstr "Mesto:"

#: ../search/Tiles/RSSFeed.cs:47
msgid "Date Viewed:"
msgstr "Datum pregleda:"

#: ../search/Tiles/TextDocument.cs:37
#, csharp-format
msgid "{0} page"
msgid_plural "{0} pages"
msgstr[0] "{0} strana"
msgstr[1] "{0} strane"
msgstr[2] "{0} strana"

#: ../search/Tiles/Tile.cs:179
msgid "Open"
msgstr "Otvori"

#: ../search/Tiles/Utils.cs:24
msgid "Applications"
msgstr "Programi"

#: ../search/Tiles/Utils.cs:28
msgid "Calendar Events"
msgstr "Događaji iz kalendara"

#: ../search/Tiles/Utils.cs:30
msgid "Folders"
msgstr "Fascikle"

#: ../search/Tiles/Utils.cs:32
msgid "Images"
msgstr "Slike"

#: ../search/Tiles/Utils.cs:34
msgid "Audio"
msgstr "Zvuk"

#: ../search/Tiles/Utils.cs:36
msgid "Video"
msgstr "Video"

#: ../search/Tiles/Utils.cs:38
msgid "Documents"
msgstr "Dokumenti"

#: ../search/Tiles/Utils.cs:40
msgid "Conversations"
msgstr "Razgovori"

#: ../search/Tiles/Utils.cs:42
msgid "Websites"
msgstr "El. mesta"

#: ../search/Tiles/Utils.cs:44
msgid "News Feeds"
msgstr "Dovodi za vesti"

#: ../search/Tiles/Utils.cs:46
msgid "Archives"
msgstr "Arhive"

#: ../search/Tiles/Utils.cs:77 ../search/Tiles/Utils.cs:159
#: ../Util/StringFu.cs:79
msgid "Yesterday"
msgstr "Juče"

#: ../search/Tiles/Utils.cs:79 ../search/Tiles/Utils.cs:161
#: ../Util/StringFu.cs:77
msgid "Today"
msgstr "Danas"

#: ../search/Tiles/Utils.cs:81 ../search/Tiles/Utils.cs:163
msgid "Tomorrow"
msgstr "Sutra"

#: ../search/Tiles/Utils.cs:179
#, csharp-format
msgid "{0} week ago"
msgid_plural "{0} weeks ago"
msgstr[0] "pre {0} nedelju"
msgstr[1] "pre {0} nedelje"
msgstr[2] "pre {0} nedelja"

#: ../search/Tiles/Utils.cs:181
#, csharp-format
msgid "In {0} week"
msgid_plural "In {0} weeks"
msgstr[0] "Za {0} nedelju"
msgstr[1] "Za {0} nedelje"
msgstr[2] "Za {0} nedelja"

#. Let's say a year and a half to stop saying months
#: ../search/Tiles/Utils.cs:183
#, csharp-format
msgid "{0} month ago"
msgid_plural "{0} months ago"
msgstr[0] "{0} mesec"
msgstr[1] "{0} meseca"
msgstr[2] "{0} meseci"

#: ../search/Tiles/Utils.cs:185
#, csharp-format
msgid "In {0} month"
msgid_plural "In {0} months"
msgstr[0] "za {0} mesec"
msgstr[1] "za {0} meseca"
msgstr[2] "za {0} meseci"

#: ../search/Tiles/Utils.cs:187
#, csharp-format
msgid "{0} year ago"
msgid_plural "{0} years ago"
msgstr[0] "pre {0} godinu"
msgstr[1] "pre {0} godine"
msgstr[2] "pre {0} godina"

#: ../search/Tiles/Utils.cs:189
#, csharp-format
msgid "In {0} year"
msgid_plural "In {0} years"
msgstr[0] "Za {0} godinu"
msgstr[1] "Za {0} godine"
msgstr[2] "Za {0} godina"

#. FIXME: We need filters for video in Beagle.
#. They should land soon when entagged-sharp gets video support.
#: ../search/Tiles/Video.cs:27
msgid "Unknown duration"
msgstr "Nepoznata dužina"

#: ../search/Tiles/WebHistory.cs:51
msgid "URL:"
msgstr "URL:"

#: ../search/Tiles/WebHistory.cs:52
msgid "Accessed:"
msgstr "Pristupano:"

#: ../search/Tray/TrayIcon.cs:32
msgid "Desktop Search"
msgstr "Pretraga radne površine"

#: ../search/Tray/TrayIcon.cs:57
msgid "No Recent Searches"
msgstr "Nema skorašnjih pretraga"

#: ../search/Tray/TrayIcon.cs:62
msgid "Recent Searches"
msgstr "Skorašnje pretrage"

#: ../search/Tray/TrayIcon.cs:78
msgid "Clear"
msgstr "Očisti"

#. Translators: the strings in TypeFilter.cs are for when the user
#. * wants to limit search results to a specific type. You don't need
#. * to give an exact translation of the exact same set of English
#. * words. Just provide a list of likely words the user might use
#. * after the "type:" keyword to refer to each type of match.
#.
#: ../search/TypeFilter.cs:36
msgid ""
"file\n"
"files"
msgstr ""
"datoteka\n"
"datoteke"

#: ../search/TypeFilter.cs:37
msgid ""
"mail\n"
"email\n"
"e-mail"
msgstr ""
"epošta\n"
"el.pošta\n"
"el.poruka\n"
"eporuka"

#: ../search/TypeFilter.cs:38
msgid ""
"im\n"
"ims\n"
"instant message\n"
"instant messages\n"
"instant-message\n"
"instant-messages\n"
"chat\n"
"chats"
msgstr ""
"brza-poruka\n"
"brze-poruke\n"
"instant-poruke\n"
"mesendžer"

#: ../search/TypeFilter.cs:39
msgid ""
"presentation\n"
"presentations\n"
"slideshow\n"
"slideshows\n"
"slide\n"
"slides"
msgstr ""
"prezentacija\n"
"prezentacije\n"
"slajd\n"
"slajdovi\n"
"pauerpoint\n"
"powerpoint"

#: ../search/TypeFilter.cs:41
msgid ""
"application\n"
"applications\n"
"app\n"
"apps"
msgstr ""
"aplikacija\n"
"program\n"
"aplikacije\n"
"programi"

#: ../search/TypeFilter.cs:42
msgid ""
"contact\n"
"contacts\n"
"vcard\n"
"vcards"
msgstr ""
"kontakti\n"
"kontakt\n"
"vizitkarta\n"
"vizitka\n"
"vizitkarte\n"
"vizitke"

#: ../search/TypeFilter.cs:43
msgid ""
"folder\n"
"folders"
msgstr ""
"fascikla\n"
"fascikle\n"
"direktorijum\n"
"direktorijumi\n"
"dir\n"
"dirovi"

#: ../search/TypeFilter.cs:44
msgid ""
"image\n"
"images\n"
"img"
msgstr ""
"slika\n"
"slike"

#: ../search/TypeFilter.cs:45
msgid "audio"
msgstr "audio"

#: ../search/TypeFilter.cs:46
msgid "video"
msgstr ""
"video\n"
"film"

#: ../search/TypeFilter.cs:47
msgid "media"
msgstr ""
"medijum\n"
"mediji\n"
"media\n"
"medij"

#: ../search/TypeFilter.cs:48
msgid ""
"document\n"
"documents\n"
"office document\n"
"office documents"
msgstr ""
"dokument\n"
"dokumenti\n"
"office"

#: ../search/TypeFilter.cs:49
msgid ""
"conversation\n"
"conversations"
msgstr ""
"razgovori\n"
"razgovor\n"
"pošta"

#: ../search/TypeFilter.cs:50
msgid ""
"web\n"
"www\n"
"website\n"
"websites"
msgstr ""
"veb\n"
"sajt\n"
"emesto\n"
"el.mesto\n"
"emesta\n"
"sajtovi\n"
"el.mesta"

#: ../search/TypeFilter.cs:51
msgid ""
"feed\n"
"news\n"
"blog\n"
"rss"
msgstr ""
"dovod\n"
"rss\n"
"blog\n"
"njuz\n"
"fid"

#: ../search/TypeFilter.cs:52
msgid ""
"archive\n"
"archives"
msgstr ""
"arhiva\n"
"arhive"

#: ../search/TypeFilter.cs:53
msgid ""
"person\n"
"people"
msgstr ""
"osoba\n"
"čovek\n"
"ljudi"

#: ../search/UIManager.cs:44
msgid "Close Desktop Search"
msgstr "Zatvori pretragu"

#: ../search/UIManager.cs:49 ../search/UIManager.cs:71
msgid "Exit Desktop Search"
msgstr "Ugasi pretragu"

#: ../search/UIManager.cs:56
msgid "_Search"
msgstr "_Traži"

#: ../search/UIManager.cs:59
msgid "_Actions"
msgstr "_Dejstva"

#: ../search/UIManager.cs:62
msgid "Sor_t"
msgstr "Po_redak"

#: ../search/UIManager.cs:65
msgid "_Help"
msgstr "_Pomoć"

#: ../search/UIManager.cs:74
msgid "_Contents"
msgstr "_Sadržaj"

#: ../search/UIManager.cs:76
msgid "Help - Table of Contents"
msgstr "Pomoć - Sadržaj"

#: ../search/UIManager.cs:80
msgid "About Desktop Search"
msgstr "O pretrazi"

#: ../search/UIManager.cs:102
msgid "_Everywhere"
msgstr "_Svuda"

#: ../search/UIManager.cs:104
msgid "Search everywhere"
msgstr "Pretraga svuda"

#: ../search/UIManager.cs:107
msgid "_Applications"
msgstr "_Programi"

#: ../search/UIManager.cs:109
msgid "Search applications"
msgstr "Pretraga programa"

#: ../search/UIManager.cs:112
msgid "_Contacts"
msgstr "_Kontakti"

#: ../search/UIManager.cs:114
msgid "Search contacts"
msgstr "Pretraga kontakata"

#: ../search/UIManager.cs:117
msgid "Ca_lendar events"
msgstr "_Događaji"

#: ../search/UIManager.cs:119
msgid "Search calendar events"
msgstr "Pretraga rokovnika"

#: ../search/UIManager.cs:122
msgid "_Documents"
msgstr "_Dokumenti"

#: ../search/UIManager.cs:124
msgid "Search documents"
msgstr "Pretraga dokumenata"

#: ../search/UIManager.cs:127
msgid "Conve_rsations"
msgstr "_Razgovori"

#: ../search/UIManager.cs:129
msgid "Search E-Mail and Instant Messaging logs"
msgstr "Pretraga el. pošte i dnevnika brzih poruka"

#: ../search/UIManager.cs:132
msgid "_Images"
msgstr "_Slike"

#: ../search/UIManager.cs:134
msgid "Search images"
msgstr "Pretraga slika"

#: ../search/UIManager.cs:137
msgid "_Media"
msgstr "_Mediji"

#: ../search/UIManager.cs:139
msgid "Search sound and video files"
msgstr "Pretraga muzike, zvukova i filmova"

#: ../search/UIManager.cs:146
msgid "Date _Modified"
msgstr "Datum _izmene"

#: ../search/UIManager.cs:147
msgid "Sort the most-recently-modified matches first"
msgstr "Pogoci koji su najskorije menjani stavljaju se na prvo mesto"

#: ../search/UIManager.cs:150
msgid "_Name"
msgstr "_Ime"

#: ../search/UIManager.cs:151
msgid "Sort matches by name"
msgstr "Pogoci se uređuju po imenu"

#: ../search/UIManager.cs:154
msgid "_Relevance"
msgstr "_Značajnost"

#: ../search/UIManager.cs:155
msgid "Sort the best matches first"
msgstr "Najbolji pogoci su prvi na spisku"

#: ../search/UIManager.cs:268
msgid "Couldn't launch web browser"
msgstr "Nije moguće pokrenuti čitač veba"

#: ../tools/Settings.cs:226
msgid "Reload configuration"
msgstr "Ponovo učitaj podešavanja"

#: ../tools/Settings.cs:227
msgid ""
"The configuration file has been modified by another application. Do you wish "
"to discard the currently displayed values and reload configuration from disk?"
msgstr ""
"Neki drugi program je izmenio datoteku sa podešavanjima. Da li želite da "
"odbacite trenutno prikazane vrednosti i ponovo učitate podešavanja sa diska?"

#: ../tools/Settings.cs:265
msgid "Select Path"
msgstr "Izbor staze"

#: ../tools/Settings.cs:284
msgid "The selected path is already selected for indexing and wasn't added."
msgstr "Izabrana staza je već određena za pretragu i zato nije dodata."

#: ../tools/Settings.cs:287
msgid ""
"The selected path wasn't added. The list contains items that supercedes it "
"and the data is already being indexed."
msgstr ""
"Izabrana staza nije dodata. Spisak sadrži stavke koje je preinačuju što "
"znači da su podaci već ubačeni u indeks."

#: ../tools/Settings.cs:298 ../tools/Settings.cs:490
msgid "Path not added"
msgstr "Staza nije dodata"

#: ../tools/Settings.cs:307
msgid "Remove obsolete paths"
msgstr "Ukloni zastarele staze"

#: ../tools/Settings.cs:308
msgid ""
"Adding this path will obsolete some of the existing include paths. This will "
"result in the removal of the old obsolete paths. Do you still wish to add it?"
msgstr ""
"Dodavanje ove staze će zastareti staze koje su već uključene. Rezultat će biti "
"uklanjanje zastarelih staza. Da li i dalje želite da je dodate?"

#: ../tools/Settings.cs:334
msgid "Remove path"
msgstr "Ukloni stazu"

#: ../tools/Settings.cs:335
msgid ""
"Are you sure you wish to remove this path from the list of directories to be "
"included for indexing?"
msgstr ""
"Da li sigurno želite da uklonite ovu stazu iz spiska fascikli koje se "
"pretražuju?"

#: ../tools/Settings.cs:364
msgid "Remove item"
msgstr "Ukloni stavku"

#: ../tools/Settings.cs:365
msgid ""
"Are you sure you wish to remove this item from the list of data to be "
"excluded from indexing?"
msgstr ""
"Da li sigurno želite da uklonite ovu stavku iz spiska podataka za "
"indeksiranje?"

#: ../tools/Settings.cs:408
msgid "Invalid host entry"
msgstr "Neispravno ime računara"

#: ../tools/Settings.cs:415
msgid "Remote host already present in the list."
msgstr "Udaljeni računar je već prisutan u spisku."

#: ../tools/Settings.cs:424
msgid "Netbeagle Node not added"
msgstr "NetBiglov čvor nije dodat"

#: ../tools/Settings.cs:440
msgid "Remove host"
msgstr "Ukloni računar"

#: ../tools/Settings.cs:441
msgid "Are you sure you wish to remove this host from the list?"
msgstr "Da li sigurno želite da uklonite ovaj računar sa spiska?"

#: ../tools/Settings.cs:462
msgid "Select path"
msgstr "Izbor staze"

#: ../tools/Settings.cs:481
msgid "The selected path is already configured for external access."
msgstr "Izabranoj stazi se već može eksterno pristupiti"

#: ../tools/Settings.cs:505
msgid "Remove public path"
msgstr "Ukloni javnu stazu"

#: ../tools/Settings.cs:506
msgid ""
"Are you sure you wish to remove this entry from the list of public paths?"
msgstr "Da li sigurno želite da uklonite ovu stazu iz spiska javnih staza?"

#: ../tools/Settings.cs:551 ../tools/Settings.cs:640 ../tools/Settings.cs:718
#: ../tools/Settings.cs:794 ../tools/Settings.cs:1133
msgid "Name"
msgstr "Ime"

#: ../tools/Settings.cs:634
msgid "Type"
msgstr "Vrsta"

#: ../tools/Settings.cs:687
msgid "Path:"
msgstr "Staza:"

#: ../tools/Settings.cs:690
msgid "Pattern:"
msgstr "Obrazac:"

#: ../tools/Settings.cs:693
msgid "Mail folder:"
msgstr "Fascikla sa el. poštom:"

#: ../tools/Settings.cs:1041
msgid "Select Folder"
msgstr "Izbor fascikle"

#: ../tools/Settings.cs:1067 ../tools/settings.glade.h:25
msgid "P_ath:"
msgstr "_Staza:"

#: ../tools/Settings.cs:1072
msgid "M_ail folder:"
msgstr "El. _pošta:"

#: ../tools/Settings.cs:1077
msgid "P_attern:"
msgstr "_Obrazac:"

#: ../tools/Settings.cs:1101
msgid "Error adding path"
msgstr "Greška pri dodavanju staze"

#: ../tools/Settings.cs:1102
msgid ""
"The specified path could not be found and therefore it could not be added to "
"the list of resources excluded for indexing."
msgstr ""
"Izabrana staza nije pronađena i zato se ne može dodati u spisak resursa koji "
"se isključuju iz indeksiranja."

#: ../tools/Settings.cs:1132
msgid "Show"
msgstr "Prikaz"

#: ../tools/settings.glade.h:1
msgid "<b>Add Remote Host</b>"
msgstr "<b>Add Remote Host</b>"

#: ../tools/settings.glade.h:2
msgid "<b>Administration</b>"
msgstr "<b>Administracija</b>"

#: ../tools/settings.glade.h:3
msgid "<b>Display</b>"
msgstr "<b>Prikaz</b>"

#: ../tools/settings.glade.h:4
msgid "<b>Exclude Resource</b>"
msgstr "<b>Isključivanje resursa</b>"

#: ../tools/settings.glade.h:5
msgid "<b>General</b>"
msgstr "<b>Opšte</b>"

#: ../tools/settings.glade.h:6
msgid "<b>Networking</b>"
msgstr "<b>Mreža</b>"

#: ../tools/settings.glade.h:7
msgid "<b>Privacy</b>"
msgstr "<b>Privatnost</b>"

#: ../tools/settings.glade.h:8
msgid "<b>Select Mail Folder</b>"
msgstr "<b>Izbor fascikle sa poštom</b>"

#: ../tools/settings.glade.h:9
msgid "Add Remote Host"
msgstr "Dodavanje udaljenog računara"

#: ../tools/settings.glade.h:10
msgid ""
"Add a remote search-enabled host you wish \n"
"to network with."
msgstr ""
"Unesite računar koji podržava spoljašnju pretragu\n"
"sa kojim želite da se povežete."

#: ../tools/settings.glade.h:12
msgid "Add any additional paths to be included for indexing."
msgstr "Dodajte još staza koje želite da indeksirate."

#: ../tools/settings.glade.h:13
msgid "Add any remote search-enabled hosts you wish to network with."
msgstr "Dodajte sve udaljene računare sa kojima želite da se povežete."

#: ../tools/settings.glade.h:14
msgid ""
"Adjust which types of results should be visible, and in what order they "
"should be presented when grouped by type."
msgstr ""
"Podesite koje vrste rezultata treba da su vidljive i u kom redosledu treba da "
"se prikazuju kada se grupišu prema vrsti."

#: ../tools/settings.glade.h:15
msgid "Allow _external access to local search services"
msgstr "Dozvoli _eksterni pristup lokalnoj pretrazi"

#: ../tools/settings.glade.h:16
msgid "Alt"
msgstr "Alt"

#: ../tools/settings.glade.h:17
msgid "Ctrl"
msgstr "Ktrl"

#: ../tools/settings.glade.h:18
msgid "Display the search window by pressing:"
msgstr "Prikazivanje prozora za pretragu pritiskom na:"

#: ../tools/settings.glade.h:19
msgid "Exclude Resource"
msgstr "Isključi resurs"

#: ../tools/settings.glade.h:20
msgid "Filename _pattern"
msgstr "Obrazac za ime _datoteke"

#: ../tools/settings.glade.h:21
msgid "I_ndex my home directory"
msgstr "_Indeksiraj moj direktorijum"

#: ../tools/settings.glade.h:22
msgid "Index data while on battery power"
msgstr "Indeksiraj i kada računar radi na baterije"

#: ../tools/settings.glade.h:23
msgid "Indexing"
msgstr "Indeksiranje"

#: ../tools/settings.glade.h:24
msgid "Networking"
msgstr "Mreža"

#: ../tools/settings.glade.h:26
msgid "Please select a resource you wish to exclude from indexing."
msgstr "Molimo da unesete ime resursa koga želite da isključite iz indeksiranja."

#: ../tools/settings.glade.h:27
msgid "Please select the mail folder you wish to exclude from indexing. "
msgstr ""
"Molimo da izaberete direktorijum sa poštom kog želite da isključite iz "
"indeksiranja."

#: ../tools/settings.glade.h:29
msgid "Search Preferences"
msgstr "Postavke pretrage"

#: ../tools/settings.glade.h:30
msgid "Select Mail Folder"
msgstr "Izbor fascikle sa poštom"

#: ../tools/settings.glade.h:31
msgid ""
"Specify any resources, such as paths, patterns, mail folders or type of "
"objects you wish to exclude from indexing."
msgstr ""
"Unesite sve resurse, poput staza, obrazaca, fascikli sa poštom ili vrste "
"objekta koje ne želite da indeksirate."

#: ../tools/settings.glade.h:32
msgid "Specify paths that should be available for external access."
msgstr "Unesite staze koje će biti dostupne za eksterni pristup."

#: ../tools/settings.glade.h:33
msgid "Type:"
msgstr "Vrsta:"

#: ../tools/settings.glade.h:34
msgid "_Address:"
msgstr "_Adresa:"

#: ../tools/settings.glade.h:35
msgid "_Allow Beagle to be run as root"
msgstr "_Dozvoli administratoru da pokreće Bigl"

#: ../tools/settings.glade.h:36
msgid "_Folder path"
msgstr "_Staza za fascikle"

#: ../tools/settings.glade.h:37
msgid "_Mail folder"
msgstr "Fascikla za _poštu"

#: ../tools/settings.glade.h:38
msgid "_Maximum number of results displayed:"
msgstr "_Najveći broj prikazanih rezultata:"

#: ../tools/settings.glade.h:39
msgid "_Start search & indexing services automatically "
msgstr "_Automatski pokreni pretragu i indeksiranje"

#: ../tools/beagle-settings.desktop.in.h:1
msgid "Configure Desktop Search & Indexing"
msgstr "Podešavanje pretrage i indeksiranja radnog okruženja"

#: ../tools/beagle-settings.desktop.in.h:2
msgid "Configure search & indexing properties"
msgstr "Podešavanje postavki pretrage i indeksiranja"

#: ../tools/beagle-settings.desktop.in.h:3
msgid "Search & Indexing"
msgstr "Pretraga i indeksiranje"

#: ../Util/Evolution.cs:45 ../Util/Evolution.cs:163
msgid "On This Computer"
msgstr "Na ovom računaru"

#: ../Util/ExifData.cs:226
msgid "Image Directory"
msgstr "Fascikla sa slikama"

#: ../Util/ExifData.cs:228
msgid "Thumbnail Directory"
msgstr "Fascikla sa sličicama"

#: ../Util/ExifData.cs:230
msgid "Exif Directory"
msgstr "Fascikla sa eksif podacima"

#: ../Util/ExifData.cs:232
msgid "GPS Directory"
msgstr "GPS fascikla"

#: ../Util/ExifData.cs:234
msgid "InterOperability Directory"
msgstr "Fascikla za udruženi rad"

#: ../Util/ExifData.cs:236
msgid "Unknown Directory"
msgstr "Nepoznata fascikla"

#. Translators: Example output: Aug 9
#: ../Util/StringFu.cs:82
msgid "MMM d"
msgstr "d MMM"

#. Translators: Example output: Aug 9, 2000
#: ../Util/StringFu.cs:85
msgid "MMM d, yyyy"
msgstr "d MMM, yyyy"

#. Translators: Example output: 11:05 AM  (note h = 12-hour time)
#: ../Util/StringFu.cs:88
msgid "h:mm tt"
msgstr "h:mm tt"

#. Translators: {0} is a date (e.g. 'Today' or 'Apr 23'), {1} is the time
#: ../Util/StringFu.cs:94
#, csharp-format
msgid "{0}, {1}"
msgstr "{0}, {1}"

#. To translators: {0} is the time of the day, eg. 13:45
#: ../Util/StringFu.cs:111
#, csharp-format
msgid "Today, {0}"
msgstr "Danas, {0}"

#. To translators: {0} is the time of the day, eg. 13:45
#: ../Util/StringFu.cs:114
#, csharp-format
msgid "Yesterday, {0}"
msgstr "Juče, {0}"

#. To translators: {0} is the number of days that have passed, {1} is the time of the day, eg. 13:45
#: ../Util/StringFu.cs:117
#, csharp-format
msgid "{0} days ago, {1}"
msgstr "pre dana: {0}, {1}"

#. Translators: Example output: January 3, 3:45 PM
#: ../Util/StringFu.cs:122
msgid "MMMM d, h:mm tt"
msgstr "d MMMM, h:mm tt"

#. Translators: Example output: March 23 2001, 10:04 AM
#: ../Util/StringFu.cs:127
msgid "MMMM d yyyy, h:mm tt"
msgstr "d MMMM yyyy, h:mm tt"

#: ../Util/StringFu.cs:137
#, csharp-format
msgid "{0} hour"
msgid_plural "{0} hours"
msgstr[0] "{0} sat"
msgstr[1] "{0} sata"
msgstr[2] "{0} sati"

#: ../Util/StringFu.cs:144
#, csharp-format
msgid "{0} minute"
msgid_plural "{0} minutes"
msgstr[0] "{0} minut"
msgstr[1] "{0} minuta"
msgstr[2] "{0} minuta"

#. Translators: {0} is a file size in bytes
#: ../Util/StringFu.cs:160
#, csharp-format
msgid "{0} bytes"
msgstr "bajtova: {0}"

#. Translators: {0} is a file size in kilobytes
#: ../Util/StringFu.cs:164
#, csharp-format
msgid "{0:0.0} KB"
msgstr "{0:0.0} KB"

#. Translators: {0} is a file size in megabytes
#: ../Util/StringFu.cs:167
#, csharp-format
msgid "{0:0.0} MB"
msgstr "{0:0.0} MB"
