using System;

namespace Search.Tiles {

	public enum TileGroup {
		Application,
		Calendar,
		Contact,
		Folder,
		Image,
		Audio,
		Video,
		Documents,
		Conversations,
		Website,
		Feed,
		Archive
	}
}
